var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')
    	.sass('admin.scss')
    	.sass('front.scss');

    mix.scripts([
	    'modernizr-2.8.3-respond-1.4.2.min.js','jquery-1.11.2.min.js', 'bootstrap.min.js','main.js'
	], 'public/js/build.min.js');
});

