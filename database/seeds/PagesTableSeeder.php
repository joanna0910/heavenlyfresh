<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'page_name' => 'Home',
            'url' => 'home',
            'page_content' => ''
        ]);

        DB::table('pages')->insert([
            'page_name' => 'About Us',
            'url' => 'about-us',
            'page_content' => ''
        ]);

        DB::table('pages')->insert([
            'page_name' => 'Rates',
            'url' => 'rates',
            'page_content' => ''
        ]);
        DB::table('pages')->insert([
            'page_name' => 'Gallery',
            'url' => 'gallery',
            'page_content' => ''
        ]);
        DB::table('pages')->insert([
            'page_name' => 'Find Us',
            'url' => 'find-us',
            'page_content' => ''
        ]);

    }
}
