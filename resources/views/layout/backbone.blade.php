
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Heavenly Fresh Resort</title>
        <meta name="description" content="Hotspring resort, heavenlyfreshresort, pansol, laguna">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Cache-control" content="public">
        <meta property="og:url"                content="http://theheavenlyfreshresort.com/" />
        <meta property="og:type"               content="website" />
        <meta property="og:title"              content="Heavenly Fresh Resort" />
        <meta property="og:description"        content="Relax and unwind with the company of your friends and family in our private resort located in Calamba Laguna. " />
        <meta property="og:image"              content="{{URL::asset('images/front/home-banner-fb.jpg')}}" />
        <meta property="og:image:height"       content="315" />
        <meta property="og:image:width"       content="600" />


        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="{{URL::asset('images/front/favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{URL::asset('images/front/favicon.ico')}}" type="image/x-icon">

        <link href='https://fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/front.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/lightbox.min.css')}}">
        <style>

        </style>

        
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="top-header ">
        <div class="container"> 
        Contact No: 0933-948-1908 / 0939-913-1030 / 0906-370-5447 / (049) 5454-2876
        </div>
    </div>
    <nav class="navbar-front-resort container" role="navigation">
        <div class="navbar-menu-button"><img src="{{URL::asset('images/front/menu.png')}}"></div>
        <div class="navbar-front-resort-logo"><img src="{{URL::asset('images/front/heavenly-fresh-logo.png')}}"></div><!--
        --><ul class="navbar-front-resort-menu navbar-closed">
            <li  ><a href="{{url('home')}}">HOME</a></li>
            <li><a href="{{url('about-us')}}">ABOUT US</a></li>
            <li><a href="{{url('rates')}}">RATES</a></li>
            <li><a href="{{url('gallery')}}">GALLERY</a></li>
            <li><a href="{{url('find-us')}}">CONTACT US</a></li>
        </ul>
    </nav>

    @yield('content')

      <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-6">&copy; Copyright 2015 Heavenly Fresh Resort</div>
                <div class="col-xs-6 helper-right"> Theme By: Joanna Lee</div>
            </div>
        </div>
      </footer>
    </div> <!-- /container -->      
        <script src="{{URL::asset('js/build.min.js')}}"></script>
        <script src="{{URL::asset('js/lightbox.min.js')}}"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-74514599-1', 'auto');
          ga('send', 'pageview');

        </script>
    </body>
</html>