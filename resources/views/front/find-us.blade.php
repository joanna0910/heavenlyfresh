
@extends('layout.backbone')


@section('content')

<div class="find-us">
	<div class="container">
		<div class="row">
			<div class="contact-left">
				<div class="contact-information">
					<div class="contact-iformation-header">Contact Information</div>
						<div class="contact-information-content">
							</br>
							B6 L6 Calachuchi St. </br>
							Solemar Del Pansol Subdivision. </br></br>
							Contact Phone: 0933-948-1908/</br>
							0939-913-1030/</br>
							0906-370-5447/</br>
							(049) 545-2876 </br>
						</div>	
						</br>
					</div>	
					</br></br>
					<div>We shall require all our guests to make a prior reservation in order for us to provide our utmost personalized service. For your inquiries/reservations, please contact us at the number above. We will inform you on the designated bank account in which to make the deposit/payment upon confirmation of reservation. Kindly call us or send us a copy of the deposit slip thru e-mail at vicky_lee2012@yahoo.com. Once we receive your deposit slip, we will contact you to confirm your reservation.</div>
			</div>
			<div class="contact-map">
				<img src="{{URL::asset('images/front/contact-map.png')}}"/>
			</div>
			</br>
		</div>
	</div>
</div>

<div class="terms">
	<div class="container">
		<div class="row">
			<div class="terms-topline"></div>
			<div class="terms-header">TERMS AND CONDITIONS</div>
			<div class="terms-bottomline"></div>
			</br></br>
			<div class="terms-content">A 50% deposit of the package cost is required to confirm the reservation.
			</br></br>
			Cancellations made before 48 hours will mean a refund of 50% of the total deposit however, beyond 48 hours will mean forfeiture of refund. Balance of reservations will be paid upon check-in.
			</br></br>
			NOTE : Acts of God (e.g. earthquakes, typhoons, floods, etc.) will mean re-scheduling / re-booking of reserved dates only! Deposits are non-refundable.
			</br></br>
			PLEASE CALL US FOR RESERVATION, and BRING DEPOSIT SLIP and PAY YOUR BALANCE UPON YOUR ENTRY AT THE RESORT.</div>
			</br></br></br>
		</div>
	</div>
</div>

@endsection
 