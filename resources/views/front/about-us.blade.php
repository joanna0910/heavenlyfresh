
@extends('layout.backbone')


@section('content')
<div class="about-video">
	<iframe  id="video" class="about-video-content" src="https://www.youtube.com/embed/WM0XgWOl4Y0?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>

</div>
<div class="container about-description">
	<div class="row">
		<div class="col-xs-12">
			Heavenly Fresh Resort is Located in Solemar del Pansol Subdivision, Calamba Laguna. It's a secured private subdivision that mostly focuses in private resort businesses. It is recently renovated to maximize the customers comfort and satisfaction thus assuring you a fun, healthy and exciting vacation.	
		</div>
	</div>	
</div>

<div class="about-amenities-title">
	<div class="container">
		<div class="row">
			Inclusions and Amenities
		</div>	
	</div>
</div>

<div class="container about-amenities-content">
	<div class="row about-amenities-row">
		<div class="col-sm-12  col-md-5">
			<div class="about-amenities-image"><img src="{{URL::asset('images/front/about-bed-icon.png')}}"></div>
			<div class="about-amenities-description"> Our fully air conditioned room ( each with/bath and toilet, 9 queen-sized beds in total </div>
		</div>
		<div class="col-sm-12 col-md-offset-1   col-md-5">
			<div class="about-amenities-image"><img src="{{URL::asset('images/front/about-multi-icon.png')}}"></div>
			<div class="about-amenities-description"> Use of Multipurpose Hall </div>
		</div>
	</div>	
	<div class="row about-amenities-row">
		<div class="col-sm-12  col-md-5">
			<div class="about-amenities-image"><img src="{{URL::asset('images/front/about-shower-icon.png')}}"></div>
			<div class="about-amenities-description"> One comfort room and shower area at the ground floor </div>
		</div>
		<div class="col-sm-12 col-md-offset-1   col-md-5">
			<div class="about-amenities-image"><img src="{{URL::asset('images/front/about-pool-icon.png')}}"></div>
			<div class="about-amenities-description"> Newly filled fresh water adult sized and kiddy pool </div>
		</div>
	</div>	
	<div class="row about-amenities-row">
		<div class="col-sm-12  col-md-5">
			<div class="about-amenities-image"><img src="{{URL::asset('images/front/about-kitchen-icon.png')}}"></div>
			<div class="about-amenities-description"> Kitchen with gas stove, refrigerator and charcoal grill (charcoal and kitchen tools not provided) </div>
		</div>
		<div class="col-sm-12 col-md-offset-1   col-md-5">
			<div class="about-amenities-image"><img src="{{URL::asset('images/front/about-karaoke-icon.png')}}"></div>
			<div class="about-amenities-description"> Unlimited use of Karaoke and billard table </div>
		</div>
	</div>	
</div>
</br>
</br>

@endsection
 