
@extends('layout.backbone')


@section('content')
	<!-- <div class="home-banner">
	</div> -->
	<img class="home-banner" src="{{URL::asset('images/front/home-banner.png')}}">
	<div class="container home-container">
		<div class="row">
			<div class="helper-center"><div class="home-welcome-topline"></div></div>
			<div class="home-welcome-header">
				WELCOME TO HEAVENLY FRESH
			</div>
			<div class="helper-center"><div class="home-welcome-bottomline"></div></div>

			<div class="home-welcome-description">
				Relax and unwind with the company of your friends and family in our private resort located in Calamba Laguna. Enjoy dipping in the pool filled with natural and clean hot spring water.
				</br></br>
				Experience the picturesque scenery, pristine air and clean surroundings at Heavenly Fresh Resort. Feast your eyes with the grand view of Mt. Makiling and get awed with its green, lush and natural feel. This nature's gift provides clean and healthy water that we use in filling our pools in Heavenly Fresh Resort.
				</br></br>
				Our package includes swimming pools which are great for day or night swims, which you, your friends or your family can enjoy a very relaxing dip. Besides swimming, the lobby has a billiard table and videoke for other means of amusement. Our resort boasts of four clean and spacious.
			</div>
		</div>

	</div>

	<div class="hotspring-background">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="home-hotspring-header">THE HOTSPRING</div>
					</br>
					<div class="home-hotspring-description">
						Los Banos which means bathing places in Spanish, is a well-known tourist destination for hot spring resorts and are frequent weekend or summer getaways for residents of the vast metropolis and tourists from other places in the Philippines and abroad.
						</br></br>
						Tourists who visit Los Banos also come to the several native delicacies stores in the town to buy the town's famous Buko pie (coconut meat pie).
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<img class="home-spring-circle1" src="{{URL::asset('images/front/home-circle1.png')}}">
					<img class="home-spring-circle2" src="{{URL::asset('images/front/home-circle2.png')}}">
					<img class="home-spring-circle3" src="{{URL::asset('images/front/home-circle3.png')}}">
				</div>
			</div>
		</div>
	</div>
@endsection
