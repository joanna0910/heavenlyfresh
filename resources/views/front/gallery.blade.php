
@extends('layout.backbone')


@section('content')

<div class="gallery">
	<div class="container">
		<div class="row">
			<div class="gallery-header-container">
				<div class="gallery-header-topline"></div>
				<p class="gallery-header">GALLERY</p>
				<div class="gallery-header-bottomline"></div>
			</div>

			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/1.JPG')}}" data-lightbox="gallery-image-set" data-title="One adult pool and one kiddie pool">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/1.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/2.JPG')}}" data-lightbox="gallery-image-set" data-title="One adult pool and one kiddie pool">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/2.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/3.JPG')}}" data-lightbox="gallery-image-set" data-title="One adult pool and one kiddie pool">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/3.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/4.JPG')}}" data-lightbox="gallery-image-set" data-title="One adult pool and one kiddie pool">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/4.JPG')}}" alt=""/>
			</a>

			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/5.JPG')}}" data-lightbox="gallery-image-set" data-title="Kitchen Area equipped with gas stove">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/5.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/6.JPG')}}" data-lightbox="gallery-image-set" data-title="Barbeque grill (charcoal not included)">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/6.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/7.JPG')}}" data-lightbox="gallery-image-set" data-title="Barbeque grill (charcoal not included)">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/7.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/8.JPG')}}" data-lightbox="gallery-image-set" data-title="Free use of karaoke">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/8.JPG')}}" alt=""/>
			</a>

			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/9.JPG')}}" data-lightbox="gallery-image-set" data-title="Multipurpose area with a television">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/9.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/10.JPG')}}" data-lightbox="gallery-image-set" data-title="Multipurpose area with a television">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/10.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/11.JPG')}}" data-lightbox="gallery-image-set" data-title="Billiard table">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/11.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/12.JPG')}}" data-lightbox="gallery-image-set" data-title="Multipurpose area with a television">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/12.JPG')}}" alt=""/>
			</a>

			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/13.JPG')}}" data-lightbox="gallery-image-set" data-title="Bedroom One">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/13.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/14.JPG')}}" data-lightbox="gallery-image-set" data-title="Bedroom Two">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/14.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/15.JPG')}}" data-lightbox="gallery-image-set" data-title="Bedroom Three">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/15.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/16.JPG')}}" data-lightbox="gallery-image-set" data-title="Bedroom Four">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/16.JPG')}}" alt=""/>
			</a>

			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/17.JPG')}}" data-lightbox="gallery-image-set" data-title="Shower rooms">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/17.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/18.JPG')}}" data-lightbox="gallery-image-set" data-title="Shower rooms">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/18.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/19.JPG')}}" data-lightbox="gallery-image-set" data-title="Shower rooms">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/19.JPG')}}" alt=""/>
			</a>
			<a class="gallery-image-link" href="{{URL::asset('images/front/gallery-images/20.JPG')}}" data-lightbox="gallery-image-set" data-title="Shower rooms">
				<img class="gallery-image" src="{{URL::asset('images/front/gallery-images/20.JPG')}}" alt=""/>
			</a>
		</div>
	</div>
</div>

@endsection
 