
@extends('layout.backbone')


@section('content')

<div class="rates">
	<div class="container">
		<div class="row">
			<p class="rates-header">Prices</p>	
			<div class="col-xs-8  col-sm-5 col-md-4 rates-category">
				Monday to Thursday
				</br></br>
				Day Stay (8 am to 5 pm)
				</br>
				Night Stay (7 pm to 6 am)
				</br></br>
				<div class="rate-price rate-price-promo">PHP 5999 <br><span>(Limited time offer through MetroDeal )</span></div>
				</br>
			</div>

			<div class="col-xs-8 col-sm-offset-1 col-sm-5 col-md-4  rates-category">
				Friday to Sunday
				</br></br>
				22 hours stay
				</br>
				8am to 6am OR 7pm to 5pm
				</br></br>
				<div class="rate-price">PHP 20000</div>
				</br>
			</div>
		</div>
	</div>
</div>

@endsection
 