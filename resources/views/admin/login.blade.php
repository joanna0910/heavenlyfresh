
@extends('layout.backbone-admin')


@section('content')
<div class="container" >

	{!!Form::open([ 'method' => 'post', 'class' => 'form-signin'])!!}
		{!! csrf_field() !!}
	    <h3 class="form-signin-heading">Please sign in</h3>
	    <label for="inputEmail" class="sr-only">Email address</label>
	    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="" name="email" value="{{ old('email') }}">
	    <label for="inputPassword" class="sr-only">Password</label>
	    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="" name="password">
	    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	{!!Form::close()!!}
 </div>
@endsection

