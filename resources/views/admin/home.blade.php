
@extends('layout.backbone-admin')


@section('content')
	@include('admin/sidebar')
	<div class="admin-home-container">

	<div class="row bootstrap-remove-margin-row">
	  	<div class="col-md-11">
	  	{!!Form::open([ 'method' => 'post', 'class' => 'resort-form' ])!!}
			{!! csrf_field() !!}
	    	<div class="form-group">
			    <label for="topBannerText">Top Banner Text</label>
			    <textarea type="text" name='topBannerText' class="form-control" id="top-banner-text" placeholder="Top Banner Text">{{$topBannerText}} </textarea>
			</div>
			<div class="form-group">
			    <label for="topHotspringText">Top Hotspring Text</label>
			    <textarea type="text" name="topHotspringText" class="form-control" id="top-hotspring-text" placeholder="Top Hotspring Text">{{$topHotspringText}}  </textarea>
			</div>
			<br/>
			<button  class="btn btn-primary pull-right" type="submit">Save</button>
		{!!Form::close()!!}
		</div>
	</div>

	</div>
@endsection

