<?php

namespace Fireflies\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use Fireflies\Http\Controllers\Controller;

class HomeController extends Controller
{

    /**
     * Insert content of home page
     *
     * @param  int  $id
     * @return Response
     */
    public function postHome(Request $request)
    {
        $input = $request->input();

        $pageData = array(
            'topBannerText' => $input['topBannerText'],
            'topHotspringText'    => $input['topHotspringText']
        ); 


        DB::table('pages')
            ->where('page_name', 'Home')
            ->update(['page_content' => json_encode($pageData)]);       

        return redirect('admin/home');
    }

    /**
     * Show Dashboard
     *
     * @param  int  $id
     * @return Response
     */
    public function showDashboard()
    {
        return view('admin.dashboard');
    }

    /**
     * Show Dashboard
     *
     * @param  int  $id
     * @return Response
     */
    public function showHome()
    {   
        $page = DB::table('pages')
            ->where('page_name', 'Home')
            ->get();

        $pageContent = json_decode($page[0]->page_content, true);

        return view('admin.home',$pageContent);
    }
}