<?php

namespace Fireflies\Http\Controllers\FrontEnd;

use DB;
use Fireflies\Http\Controllers\Controller;

class FindUsController extends Controller
{
	/**
     * Show frontend home page
     *
     * @return view
     */
	public function showFindUs(){
		/*$page = DB::table('pages')
            ->where('page_name', 'Home')
            ->get();

        $homeContent = json_decode($page[0]->page_content, true);*/
        return view('front.find-us');
	}
}