<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'auth'], function () {
    Route::get('admin/dashboard', 'Admin\HomeController@showDashboard');
    Route::get('admin/home', 'Admin\HomeController@showHome');
    Route::post('admin/home', 'Admin\HomeController@postHome');
});


Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@logout');


Route::get('/', 'FrontEnd\HomeController@showHome');
Route::get('home', 'FrontEnd\HomeController@showHome');
Route::get('about-us', 'FrontEnd\AboutUsController@showAboutUs');
Route::get('rates', 'FrontEnd\RatesController@showRates');
Route::get('gallery', 'FrontEnd\GalleryController@showGallery');
Route::get('find-us', 'FrontEnd\FindUsController@showFindUs');
